Beta-adic tools
---------------
This package implements beta-adic sets, i.e. sets of the form
{ a_0 + a_1*b + a_2*b^2 + ... + a_n*b^n | n in N, a_0 a_1 ... a_n in L }
where b is a number, and L is a regular language (over an alphabet in the same ring as b).
It also implements efficient automata written in C language (mainly deterministic automtata).
