# setup.py : file that explains what need to be installed
#
# pip: python package manager
#   pip install xxx
#   pip install xxx
#
# sage -pip install --user --upgrade .
#
# sage -pip install --user --editable . # to prevent sage to copy sources

from __future__ import print_function, absolute_import

from distutils.core import setup
from Cython.Build import cythonize
from distutils.extension import Extension
from os import system
#from warnings import warn
import warnings
from setuptools.command.test import test as TestCommand # for tests

SDL_present = True
system("/usr/bin/gcc test_SDL2.c -lSDL2 2> libsdl.txt")
with open("libsdl.txt", 'r') as f:
    r = f.read()
    print("r = ((%s))" % r)
    if r != "":
        SDL_present = False
        warnings.warn("The library SDL2 is not present in your system. You will not be able to use full functionnalities.", Warning)
        #raise ValueError("SDL2 library must be installed in order to install this package !")

SDL_image_present = True
system("/usr/bin/gcc test_SDL2_image.c -lSDL2 2> libsdl.txt")
with open("libsdl.txt", 'r') as f:
    r = f.read()
    print("r = ((%s))" % r)
    if r != "":
        SDL_image_present = False
        warnings.warn("The library SDL2_image is not present in your system. You will not be able to use full functionnalities.", Warning)
        #raise ValueError("SDL2 library must be installed in order to install this package !")

def bool_to_int(b):
    if b:
        return 1
    return 0
    
def readfile(filename):
    with open(filename, "r") as f:
        return f.read()

# For the tests
class SageTest(TestCommand):
    def run_tests(self):
        errno = os.system("sage -t --force-lib badic")
        if errno != 0:
            sys.exit(1)

sourcefiles = ['badic/cautomata.pyx', 'badic/automataC.c']
sourcefiles2 = ['badic/beta_adic.pyx', 'badic/complex.c', 'badic/draw.c', 'badic/relations.c', 'badic/automataC.c']

extensions = [
              Extension("badic.cautomata", sourcefiles,
              depends = ['badic/Automaton.h']),
              
              Extension("badic.beta_adic", sourcefiles2,
              libraries=['stdc++', 'SDL2'],
              #include_dirs=['/usr/local/include'],
              depends = ['badic/draw.h', 'badic/Automaton.h', 'badic/relations.h', 'badic/complex.h'],
              define_macros=[('SDL_PRESENT', bool_to_int(SDL_present)), ('SDL_IMAGE_PRESENT', bool_to_int(SDL_image_present))])]

setup(
    name='badics',
    packages=['badic/'],
    description='Beta-adics and automata tools',
    long_description = readfile("README.txt"), # get the long description from the README
    url='https://gitlab.com/mercatp/badic',
    author='Paul Mercat, Dominique Benielli',
    author_email='paul.mercat@univ-amu.fr', # choose a main contact email
    license='GPLv3.0', # This should be consistent with the LICENCE file
    classifiers=[
      # How mature is this project? Common values are
      #   3 - Alpha
      #   4 - Beta
      #   5 - Production/Stable
      'Development Status :: 4 - Beta',
      'Intended Audience :: Science/Research',
      'Topic :: Software Development :: Build Tools',
      'Topic :: Scientific/Engineering :: Mathematics',
      'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
      'Programming Language :: C, Python :: 2.7',
    ], # classifiers list: https://pypi.python.org/pypi?%3Aaction=list_classifiers
    keywords = "SageMath beta-adic automata",
    cmdclass = {'test': SageTest}, # adding a special setup command for tests
    ext_modules=cythonize(extensions)
)

